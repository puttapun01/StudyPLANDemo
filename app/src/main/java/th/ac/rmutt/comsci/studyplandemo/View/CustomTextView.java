package th.ac.rmutt.comsci.studyplandemo.View;

import android.app.Application;

import th.ac.rmutt.comsci.studyplandemo.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Puttapan on 5/5/2560.
 */

public class CustomTextView extends Application {

    @Override
    public void onCreate(){
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/thaisanslite.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}