package th.ac.rmutt.comsci.studyplandemo;

/**
 * Created by Puttapan on 5/5/2560.
 */

public class User {
    String userId;
    String userUsername;
    String userPassword;
    String userStatus;

    public User(){

    }

    public User(String userId, String userUsername, String userPassword, String userStatus) {
        this.userId = userId;
        this.userUsername = userUsername;
        this.userPassword = userPassword;
        this.userStatus = userStatus;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserUsername() {
        return userUsername;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getUserStatus() {
        return userStatus;
    }
}
