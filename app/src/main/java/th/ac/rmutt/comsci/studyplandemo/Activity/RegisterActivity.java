package th.ac.rmutt.comsci.studyplandemo.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import th.ac.rmutt.comsci.studyplandemo.R;
import th.ac.rmutt.comsci.studyplandemo.User;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends AppCompatActivity {

    EditText etUsername;
    EditText etPassword;
    EditText etConPassword;
    Spinner spnStatus;
    TextView btnRegister;

    DatabaseReference databaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        databaseUser = FirebaseDatabase.getInstance().getReference("user");

        // คำสั่งซ่อน Status Bar

        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        // คำสั่งแสดง Status Bar

        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConPassword = (EditText) findViewById(R.id.etConPassword);
        spnStatus = (Spinner) findViewById(R.id.spnStatus);
        btnRegister = (TextView) findViewById(R.id.btnRegister);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addUser();
            }
        });

    }

    private void addUser() {
        String username = etUsername.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String conpassword = etConPassword.getText().toString().trim();
        String status = spnStatus.getSelectedItem().toString();

        if(!username.isEmpty() && !password.isEmpty() && !conpassword.isEmpty()){
            String id = databaseUser.push().getKey();
            User user = new User(id, username, password, status);
            databaseUser.child(id).setValue(user);
            Toast.makeText(this, "เพิ่มข้อมูล", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        else if (!password.equals(conpassword)){
            Toast.makeText(this, "รหัสไม่ตรงกัน", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, "กรุณาใส่ข้อมูลให้ครบ", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void attachBaseContext (Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
